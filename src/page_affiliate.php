<?php
/*
Template Name: Affiliate Template
*/
?><!DOCTYPE html>

<html <?php language_attributes(); ?>>

<head>
  <?php get_header(); ?>

</head>

<body>
  <div class="mh-wrapper mh-clearfix">
    <main id="main-content" class="page-xl w-full mx-auto juttu bg-white" itemprop="mainContentOfPage" tabindex="-1">
      <header class="entry-header bg-primary py-4 mb-4">
        <h1 class="entry-title page-title text-white text-center"><?php the_title(); ?></h1>
      </header>

      <?php include (dirname( __FILE__ ) . '/booker_info.php'); ?>
    </main>
  </div>
  <?php get_footer(); ?>
</body>
