<?php
?>
<head>
  <style type="text/css">
  img.emoji {
      font-size: 2.5rem;
  }
  .content-text {
    max-width: 768px;
  }
  .table-elem {
     background-color: #114e7d;
  }
  .table-elem:nth-child(odd) {
     background-color: rgb(40,80,150);
  }
  .bg-cta {
     background-color: #f07d00;
  }
  </style>
</head>

<?php
  $logo_id = get_field('logo');
  $logo = wp_get_attachment_image( $logo_id, '185x50', false, array('class' => 'mx-auto') ); // logos are always this size
?>

<div class="flex flex-col md:flex-row flex-wrap justify-between mb-4">
  <!-- logo, cta -->
  <div class="flex-0 mx-4 mb-10">
    <div class="w-full max-w-xs mx-auto mb-10">
      <?= $logo; ?>
    </div>
    <a class="block text-center text-2xl font-bold uppercase bg-cta text-white px-4 py-10 border-black border-2 rounded-md text-black hover:bg-blue-700 hover:text-white"
       href="<?php the_field('cta'); ?>">
        <?php the_field('cta_text'); ?>
    </a>

    <!-- languages -->
    <?php
      $flags = array (
        "Finnish" => "&#127467;&#127470;",
        "English" => "&#127468;&#127463;"
      );
      $langs = get_field('languages');
      if($langs):
    ?>
    <ul class="mt-4 flex flex-row text-xl">
      Kielet:
      <?php foreach( $langs as $lang ): ?>
        <li class="ml-10"><?php echo($flags[$lang]); ?></li>
      <?php endforeach; ?>
    </ul>
    <?php endif; ?>
  </div>

  <!-- info -->
  <div class="mx-4">
    <?php if (get_field('freebet')): ?>
      <div class="text-2xl text-center font-semibold mb-4 lg:mb-10">Ilmaisveto: <?php the_field('freebet'); ?></div>
    <?php else: ?>
      <div class="text-2xl text-center font-semibold mb-4 lg:mb-10"><?php the_field('deposit'); ?> &euro; / <?php the_field('bonus'); ?> % bonus</div>
      <div class="text-lg font-bold">Kierrätys: <?php the_field('circulation'); ?>-kertainen (<?php the_field('t_b'); ?>)</div>
      <div class="text-lg font-bold">Kierrätyskerroin: <?php the_field('multiplier'); ?></div>
    <?php endif; ?>
    <div class="text-lg font-bold">Bonus voimassa: <?php the_field('expire_days'); ?> pv</div>
    <div class="text-lg font-bold">Minimitalletus: <?php the_field('min_deposit'); ?> &euro;</div>
    <?php if (get_field('max_bet')): ?>
      <div class="text-lg font-bold">Maksimipanos: <?php the_field('max_bet'); ?> &euro; / veto</div>
    <?php endif; ?>

    <?php if (have_rows('features')): ?>
    <table class="mt-4 w-full border-none">
      <?php
      while ( have_rows('features') ):
        the_row();
        $arr = get_row();
        foreach (array_keys($arr) as $elem):
          $obj = get_sub_field_object($elem);
          $name = $obj['label'];
          $value = $obj['value'];
      ?>
          <tr class="m-2 border-none">
            <td class="border-none m-0 p-0"><?php echo $name; ?></td>
            <td class="border-none m-0 p-0"><?= affi_boolean_icon($value); ?></td>
          </tr>
        <?php endforeach; ?>
      <?php endwhile; ?>
    </table>
    <?php endif; ?>
  </div>

  <!-- review -->
  <div class="mx-4">
  <?php
  if (have_rows('review')) {
    while ( have_rows('review') ) {
      the_row();
      ?>
      <table class="w-full">
      <?php
      $arr = get_row();
      foreach (array_keys($arr) as $elem):
          $obj = get_sub_field_object($elem);
          $name = $obj['label'];
          $value = $obj['value'];
        ?>
        <tr class="table-elem text-white">
          <td class="uppercase font-semibold p-4 pr-10"><?php echo $name; ?></td>
          <td class="font-semibold p-4 text-right"><?php echo $value; ?> / 5</td>
        </tr>
        <?php endforeach; ?>
      </table>
      <?php
    }
  }
  ?>
  </div>
</div>

<div class="content-text mb-4">
  <div class="mx-4">
    <h4 class="text-bold text-xl mb-2 lg:mb-4">Huomioitavaa bonuksessa</h4>
    <p><?php the_field('huom'); ?></p>
  </div>
</div>

<div class="mx-4">
  <div class="block lg:hidden sticky top-0">
    <a class="block text-center text-2xl font-bold uppercase bg-cta text-white px-4 py-10 border-black border-2 rounded-md text-black hover:bg-blue-700 hover:text-white"
       href="<?php the_field('cta'); ?>">
        <?php the_field('cta_text'); ?>
    </a>
  </div>
  <?php
  while (have_posts()) :
    the_post();
    get_template_part('content', 'page');
  endwhile;
  ?>
</div>


