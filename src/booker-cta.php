<?php
global $post;
$bookers = json_decode(html_entity_decode($post->vihje_bookers));

if (!$bookers) {
  return;
}

// clear out empty elements
$bookers = array_filter( $bookers, function ($b) { return $b->booker; } );

// TODO filter out same ids (filter) (if the same booker is twice here)

// sort by multiplier
usort( $bookers, function ($a, $b) {
  if ( $a->kerroin === $b->kerroin ) {
    return 0;
  }
  return ($a->kerroin < $b->kerroin) ? 1 : -1;
});

if (count($bookers) > 0):
?>

<?php // Draw the expire info ?>
<?php
// Add timezone info: all our times are from Helsinki
$tz = new DateTimeZone('Europe/Helsinki');
$expires = date_create($post->vihje_expires, $tz);
$now = new DateTime('now', $tz);
if ( $expires > $now ):
?>
  <div class="text-green-700 font-bold text-lg flex justify-between mx-4">
    <div>Sulkeutuu</div>
    <div class="inline-block text-right">
      <?php
      echo date_format($expires, "j.m.Y") . " klo " . date_format($expires, "H:i");
      ?>
    </div>
  </div>
<?php else: ?>
  <div class="text-red-500 font-bold text-xl mx-4 text-right">
    Sulkeutunut
  </div>
<?php endif; ?>

<div class="flex flex-col m-2">
  <?php // Draw the header ?>
  <div class="flex flex-row border-none bg-primary w-full">
    <div class="text-white font-bold text-xl mx-2">
      <?= $post->vihje_game; ?>
    </div>
    <div class="mx-auto"></div>
    <div class="w-4/12 md:w-1/2 text-white font-bold text-xl text-right mx-2">
      <span class="hidden lg:inline">Panostus: </span><?= $post->vihje_panostus; ?>
    </div>
  </div>

  <?php // Draw the Booker rows ?>
  <?php $first = true; ?>
  <?php foreach ($bookers as $b):
    // booker is a group not an individual post
    $grp = $b->booker;
    $posts = get_posts([
      'post_type' => 'booker',
      'numberposts' => -1,
      'booker_group' => $grp
    ]);
    global $post;
    foreach ($posts as $post):
      set_query_var('is_first_element', $first);
      set_query_var('game_multiplier', $b->kerroin);

      include (dirname( __FILE__ ) . '/booker-row-template.php');

      // clear variables
      set_query_var('game_multiplier', null);
      set_query_var('is_first_element', null);
      $first = false;
    endforeach;

  endforeach; ?>
</div>

<?php wp_reset_postdata(); ?>

<?php endif; ?>
