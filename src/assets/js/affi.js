// TODO add hiding if user clicks outside the modal
const modalVisible = (name) => {
  document.body.style.overflowY='hidden';

  // hide on esc
  jQuery(document).keydown(function(e) {
    if (e.key === 'Escape') {
      const modal = jQuery(name).filter(':visible');
      // click the cancel button so the CSS can keep up
      const cancel = modal.find('#cancel');
      if (cancel.length > 0) {
        cancel[0].click();
      }
      // Unbind the event
      jQuery(document).off('keydown');
    }
   });
}

const modalHidden= () => {
   document.body.style.overflowY='auto';
}
