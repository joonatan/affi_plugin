jQuery(document).ready(function() {
  jQuery('.editinline').on('click', function() {
    const tag_id = jQuery(this).parents('tr').attr('id');
    // need to query class since there is no text
    // .osu .huti .push or .osuma-not-set
    // .osuma-not-set don't need to do anything
    const osuko = jQuery('.osuko span', '#'+tag_id).attr('class');
    // Clear selection since this has memory
    jQuery("#osuko option").attr("selected", false);
    if (osuko.length > 0) {
      jQuery(`#osuko option[value=${osuko}]`).attr('selected', 'selected');
    }

    // Set the game name
    const games = jQuery('input#vihje-game');
    const game_val = jQuery('.game', '#'+tag_id).text();
    if (games.length > 0) {
      games.val(game_val);
    }
  });
});
