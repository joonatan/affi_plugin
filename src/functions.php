<?php
/**
* Plugin Name: Affi
* Plugin URI:
* Description: Bookers and clues for sports betting
* Version: 1.1
* Author: Joonatan Kuosa
* Author URI:
**/

include_once('booker-type.php');
include_once('vihje-type.php');

function affi_scripts() {
  wp_register_style('uv-affi', plugins_url( 'affi_style.css', __FILE__ ));
  wp_enqueue_style('uv-affi');
  wp_enqueue_script( 'affi-js', plugins_url( 'assets/js/affi.js', __FILE__  ) );
}
add_action('wp_enqueue_scripts', 'affi_scripts', 5);

// Redirect our types to template pages
function affi_single_template($single) {
  global $post;
  if ('vihje' == $post->post_type) {
    return plugin_dir_path( __FILE__ ) . '/single-vihje.php';
  } else if ('booker' == $post->post_type) {
    return plugin_dir_path( __FILE__ ) . '/page_affiliate.php';
  }
  return $single;
}
add_filter('single_template', 'affi_single_template');

function affi_booker_cta () {
  include (dirname( __FILE__ ) . '/booker-cta.php');
}

function affi_booker_info() {
  include (dirname( __FILE__ ) . '/booker_info.php');
}

function affi_boolean_icon($value) {
  ob_start();

  // TODO this needs to be scaled 1.5x or 2x
  if ($value): ?>
  <svg class="inline-block" width="30" height="26" style="fill: rgb(0, 127, 0);">
  <g>
    <rect width="26" height="5" transform="translate(6, 19) rotate(-45)"/>
    <rect width="12" height="5" transform="translate(2, 7) rotate(45)"/>
  </g>
</svg>
  <?php else: ?>
  <svg class="inline-block" width="26" height="26" style="fill: rgb(127, 0, 0);">
    <g>
      <rect width="30" height="5" transform="translate(0, 21) rotate(-45)"/>
      <rect width="30" height="5" transform="translate(3, 0) rotate(45)"/>
    </g>
  </svg>
  <?php endif;

  return ob_get_clean();
}

// Override canonical url for vihje archive
function vihjeet_archive_canonical($canonical) {
  if ( is_post_type_archive( 'vihje' ) ) {
    $link = get_post_type_archive_link('vihje');
    return $link;
  } else {
    return $canonical;
  }
}
add_filter( 'rank_math/frontend/canonical', 'vihjeet_archive_canonical' );
