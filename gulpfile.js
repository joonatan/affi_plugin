const postcss = require('gulp-postcss');
const gulp = require('gulp');
const { series } = require('gulp');
const copy = require('gulp-copy');

const concat = require('gulp-concat');
const tar = require('gulp-tar');
const gzip = require('gulp-gzip');

const run = require('gulp-run');

const OUTPUT_DIR = 'build'        // output directory for transpiled code
const SRC_DIR = 'src'             // source code directory
const PKG_NAME = 'affi_plugin';

const package = () => (
  gulp.src(`${OUTPUT_DIR}/**/*`)
      // FIXME this doesn't compress sub directories (like template-parts)
      .pipe(tar(`${PKG_NAME}.tar`))
      .pipe(gzip())
      .pipe(gulp.dest('dist'))
)

const css = () => (
  gulp.src(`${SRC_DIR}/*.css`)
    .pipe(postcss([
      require('postcss-nesting'),
      require('tailwindcss'),
      require('autoprefixer'),
    ]))
    .pipe(concat({ path: 'affi_style.css', stat: { mode: 0666 }}))
    .pipe(gulp.dest(OUTPUT_DIR))
)

const copyFiles = () => (
  gulp
    .src([`${SRC_DIR}/**/*.php`, `${SRC_DIR}/wp-admin/**/*.*`])
    .pipe(copy(OUTPUT_DIR, { prefix: 1 }))
)

const copyAssets = () => (
  gulp
    .src([`${SRC_DIR}/assets/**/*.*`])
    .pipe(copy(OUTPUT_DIR, { prefix: 1 }))
)

const copyDocker = () => {
  return run('./cp_docker.sh').exec();
}

exports.css = css
exports.default = series(css, copyFiles, copyAssets, copyDocker)
exports.package = series(css, copyFiles, copyAssets, package)
