<?php
/*  Template part to draw the booker info as a flex row
 *  creates a modal for more information for the row
 *
 *  @author Joonatan Kuosa
 *
 *  Params that are set with query_var
 *  @param game_multiplier : integer disabled if 0
 *    otherwise insert the multipler as first element
 *  @param booker_index: integer disabled if not set
 *    otherwise adds a row index number as long as game_multiplier is not set
 *  @param is_first_element : boolean if true use bigger cta style
 *  @param small: boolean if true use the small layout
 */
?>

<?php
  $is_first_element = get_query_var('is_first_element', false);
  $game_multiplier = get_query_var('game_multiplier', 0);
  $index = get_query_var('booker_index', null);
  $is_small = get_query_var('is_small', false);

  $url = get_field('cta');
  $logo_id = get_field('logo');
  $circ = get_field('circulation');
  $booker_mult = get_field('multiplier');
  $deposit = get_field('deposit');
  $bonus = get_field('bonus');
  $t_b = get_field('t_b');
  $freebet = get_field('freebet');
  $freebet2 = get_field('freebet2');

  $expire_days = get_field('expire_days');
  $min_deposit = get_field('min_deposit');
  $max_bet = get_field('max_bet');
  $huom = get_field('huom');

  $CTA_LABEL = "Pelaa";
  $SEC_LABEL = "Bonus&shy;ehdot";

  $ID = get_the_ID();

  $is_tax_free = false;
  while ( have_rows('features') ) {
    the_row();
    $arr = get_row();
    foreach (array_keys($arr) as $elem) {
      $obj = get_sub_field_object($elem);
      if (strtolower($obj['label']) == 'verovapaa') {
        $is_tax_free = $obj['value'];
      }
    }
  }

  $logo = wp_get_attachment_image( $logo_id, '185x50', false, array('class' => 'mx-auto') ); // logos are always this size
?>
  <div class="w-full flex flex-row items-center booker-table-bg">

    <div id="bookerModal-<?= $ID; ?>" class="booker-modal">
      <div class="rounded-md border-2 border-solid border-secondary">
        <div class="p-4">
          <h2 class="text-2xl font-bold">
            <?= $logo; ?>
          </h2>
          <?php if ($freebet): ?>
          <div class="text-2xl text-center font-semibold mb-2 lg:mb-4">
            <?= $freebet; ?>
            <?php if (!empty($freebet2)): ?>
              <br /> <?= $freebet2; ?>
            <?php endif ?>
          </div>
          <?php else: ?>
            <div class="text-2xl text-center font-semibold mb-2 lg:mb-4"><?= $deposit; ?> &euro; / <?= $bonus; ?> % bonus</div>
            <div class="text-lg font-semibold">Kierrätys: <?= $circ; ?>-kertainen (<?= $t_b; ?>)</div>
            <div class="text-lg font-semibold">Kierrätyskerroin: <?= $booker_mult; ?></div>
          <?php endif; ?>
          <div class="text-lg font-semibold">Bonus voimassa: <?= $expire_days; ?> pv</div>
          <div class="text-lg font-semibold">Minimitalletus: <?= $min_deposit; ?> &euro;</div>
          <?php if ($max_bet): ?>
            <div class="text-lg font-semibold mb-2">Maksimipanos: <?= $max_bet; ?> &euro; / veto</div>
          <?php endif; ?>
          <div class="text-lg font-semibold mb-2 flex justify-center align-center">
            <div class="mr-2">verovapaa</div>
            <div><?= affi_boolean_icon($is_tax_free); ?></div>
          </div>
          <p class="mb-4"><?= $huom; ?></p>
          <div class="flex">
            <a id="cancel" class="w-1/2 border-2 border-secondary text-secondary hover:bg-gray-200 hover:text-secondary flex mx-2" href="#ok"
               onclick="modalHidden();">
              <span class="w-full self-center font-semibold text-xl uppercase underline px-4">Sulje</span>
            </a>
            <a id="cta"
               class="w-1/2 btn-cta bg-cta text-white darken-on-hover hover:text-white flex mx-2"
               title="Pelaa" href="<?= $url; ?>"
               target="_blank" rel="nofollow"
               onclick="modalHidden();">
               <span class="w-full self-center text-center text-2xl font-bold uppercase px-4">
                Pelaa
              </span>
            </a>
          </div>
        </div>
      </div>
    </div>


    <!-- multiplier -->
    <?php $multi_cls = ($is_first_element) ? 'multiplier text-xl lg:text-2xl' : 'text-lg'; ?>
    <?php if ($game_multiplier > 0): ?>
    <div class="w-auto font-bold text-black">
      <span class="<?= $multi_cls; ?>"><?= number_format($game_multiplier, 2); ?></span>
    </div>
    <?php elseif ($index): ?>
    <div class="w-auto min-w-orderly font-bold text-black mr-auto">
      <span class="mx-1 text-md"><?= $index; ?></span>
    </div>
    <?php endif; ?>

    <!-- booker logo -->
    <div class="w-auto flex-shrink px-2 <?= $is_first_element ? 'mx-auto' : ''; ?>">
      <?= $logo; ?>
    </div>

    <!-- booker info -->
    <?php $info_markers_cls = ($is_small) ? 'hidden' : 'hidden md:inline' ?>
    <?php if (!$is_first_element): ?>
      <div class="w-5/12 font-semibold text-center text-md py-1">
        <?php if ($freebet): ?>
          <div class="whitespace-nowrap"><?= $freebet; ?></div>
          <?php if (!empty($freebet2)): ?>
            <div class="whitespace-nowrap"><?= $freebet2; ?></div>
          <?php endif ?>
        <?php else: ?>
          <span class="whitespace-nowrap <?= ($is_small) ? '' : 'info-section'; ?>">
            <?= $deposit; ?>&euro; / <?= $bonus; ?>%
          </span>
          <span class="whitespace-nowrap">
            <?= $circ; ?>x <?= $t_b; ?>
            | <?= number_format($booker_mult, 2); ?>
          </span>
        <?php endif; ?>
      </div>
    <?php endif; ?>

    <?php $sec_btn_cls = ($is_small ? '' : 'underline'); ?>
    <a class="w-auto self-stretch text-center text-md lg:text-lg uppercase font-semibold text-secondary px-2 hover:text-secondary hover:bg-gray-300 <?= $sec_btn_cls; ?> flex items-center "
       href="#bookerModal-<?= $ID; ?>"
       aria-label="lisätietoa"
       onclick="<?= 'modalVisible(\'#bookerModal-' . $ID . '\')'; ?>;">
        <?php if ($is_small): ?>
          <span class="material-icons">help</span>
        <?php else: ?>
          <span class="hidden lg:block"><?= $SEC_LABEL; ?></span>
          <!-- @todo arial-label for mobile -->
          <!-- have to use style since the mh-theme overrides font-size -->
          <span class="block lg:hidden">
            <span class="material-icons">help</span>
          </span>
        <?php endif; ?>
    </a>

    <!-- cta -->
    <?php $cta_cls = ($is_first_element) ? 'text-2xl px-10' : 'text-xl my-1 px-2 md:px-4'; ?>
    <a class="w-auto min-w-cta flex-shrink-0 btn-cta bg-cta text-center text-white font-bold uppercase flex darken-on-hover hover:text-white"
       href="<?= $url; ?>" target="_blank" rel="nofollow"
       >
       <span class="w-full self-center <?= $cta_cls; ?>" ><?= $CTA_LABEL; ?></span>
    </a>
  </div>

  <?php if ($is_first_element): ?>
  <div class="flex flex-row justify-between items-center booker-table-bg p-1">
    <div class="mx-auto">Kerroin</div>
    <div class="mx-auto">Booker</div>
    <div class="mx-auto">Bonus | Kierrätys | Min. Kerroin</div>
    <div class="mx-auto"></div>
    <div class="mx-auto"></div>
  </div>
  <?php endif; ?>
