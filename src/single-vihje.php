<?php
/*  Vihje template
*/
?><!DOCTYPE html>

<html <?php language_attributes(); ?>>

<?php get_header(); ?>

<body>
<div class="mh-wrapper mh-clearfix" id="all-content-wp">
  <div id="main-content" class="mh-content juttu" role="main" itemprop="mainContentOfPage">
    <?php
      while (have_posts()) :
        the_post();
        // get the theme part for posts
        get_template_part('content', 'single');
        // get the plugin part of it
        include (dirname( __FILE__ ) . '/booker-cta.php');
      endwhile;
    ?>
  </div>
  <?php get_sidebar("single"); ?>
</div>
</body>

<?php get_footer(); ?>
