<?php
/*  Adds booker post type to the theme
 *
 *  @author Joonatan Kuosa
 */

function affi_create_booker_category() {
  $labels = array(
    'name'                       => _x( 'Booker groups', 'taxonomy general name', 'affi' ),
    'singular_name'              => _x( 'Booker group', 'taxonomy singular name', 'affi' ),
    'search_items'               => __( 'Search Booker groups', 'affi' ),
    'popular_items'              => __( 'Popular Booker groups', 'affi' ),
    'all_items'                  => __( 'All Booker groups', 'affi' ),
    'parent_item'                => null,
    'parent_item_colon'          => null,
    'edit_item'                  => __( 'Edit Booker group', 'affi' ),
    'update_item'                => __( 'Update Booker group', 'affi' ),
    'add_new_item'               => __( 'Add New Booker group', 'affi' ),
    'new_item_name'              => __( 'New Booker group', 'affi' ),
    'add_or_remove_items'        => __( 'Add or remove Booker group', 'affi' ),
    'choose_from_most_used'      => __( 'Choose from the most used Booker group', 'affi' ),
    'not_found'                  => __( 'No Booker group found.', 'affi' ),
    'menu_name'                  => __( 'Booker groups', 'affi' ),
  );

  $args = array(
    'hierarchical'          => false,
    'labels'                => $labels,
    'show_ui'               => true,
    'show_admin_column'     => true,
    'update_count_callback' => '_update_post_term_count',
    'query_var'             => true,
  );

  register_taxonomy( 'booker_group', 'booker', $args );
}
add_action( 'init', 'affi_create_booker_category', 0 );

function affi_booker_post_type() {

  $labels = array(
    'name'                => _x( 'Booker', 'Post Type General Name', 'affi' ),
    'singular_name'       => _x( 'Booker', 'Post Type Singular Name', 'affi' ),
    'menu_name'           => __( 'Bookers', 'affi' ),
    'parent_item_colon'   => __( 'Parent Booker', 'affi' ),
    'all_items'           => __( 'All Booker', 'affi' ),
    'view_item'           => __( 'View Booker', 'affi' ),
    'add_new_item'        => __( 'Add New Booker', 'affi' ),
    'add_new'             => __( 'Add New', 'affi' ),
    'edit_item'           => __( 'Edit Booker', 'affi' ),
    'update_item'         => __( 'Update Booker', 'affi' ),
    'search_items'        => __( 'Search Booker', 'affi' ),
    'not_found'           => __( 'Not Found'),
    'not_found_in_trash'  => __( 'Not found in Trash'),
  );

  $args = array(
    'label'               => __( 'bookers', 'affi' ),
    'description'         => __( 'Bookers', 'affi' ),
    'labels'              => $labels,
    'supports'            => array( 'title', 'editor', 'thumbnail', 'revisions', 'custom-fields' ),
    'hierarchical'        => false, // no parents
    'public'              => true,
    'menu_position'       => 19,
    'has_archive'         => false, // no archive so we can have page at slug
    'capability_type'     => 'page',
    'show_in_rest'        => true,
    'menu_icon'           => 'dashicons-money-alt',
    'rewrite'             => array( 'slug' => 'vedonlyontisivustot' ),
  );

  register_post_type( 'booker', $args );
}
add_action( 'init', 'affi_booker_post_type', 0 );

// Add custom columns to booker
define( 'POS_KEY_NAME', 'position');
define( 'ID_KEY_NAME', 'id');

function add_to_booker_table( $column ) : array {
  $new = array();

  // add Position before date (could be after title too)
  // inserting to associate array
  foreach($column as $key=>$value) {
    if($key=='date') {
     $new[POS_KEY_NAME] = 'Position';
     $new[ID_KEY_NAME] = 'ID';
    }
    $new[$key]=$value;
  }
  return $new;
}
add_filter( 'manage_booker_posts_columns', 'add_to_booker_table');

function add_to_booker_table_row( string $column_name, $post_id ) {
  if ($column_name === POS_KEY_NAME) {
    echo get_field(POS_KEY_NAME, $post_id);
  }
  if ($column_name === ID_KEY_NAME) {
    echo $post_id;
  }
}
add_filter( 'manage_booker_posts_custom_column', 'add_to_booker_table_row', 10, 3 );

/// -------------------------------- Add shortcodes --------------------------
function booker_list_shortcode ($attr) {
  $ids = [];
  if ($attr && array_key_exists('ids', $attr)) {
    // parse the ids array into ids
    $ids = array_map(
      function (string $x) { return trim($x); },
      explode(',', $attr['ids'])
    );
  }

  $posts = get_posts([
    'post_type' => 'booker',
    'post_status' => 'publish',
    'numberposts' => -1,
    'include' => $ids,
  ]);

  // Order

  if (!empty($ids)) {
    // ids order
    usort( $posts, function ($a, $b) use($ids) {
      $key1 = array_search($a->ID, $ids);
      $key2 = array_search($b->ID, $ids);
      if ($key1 !== false && $key2 !== false) {
        return $key1 === $key2 ? 0 : (
          $key1 < $key2 ? -1 : 1
        );
      }
    });
  } else {
    // Position variable defined for every booker
    usort( $posts, function ($a, $b) {
      $p1 = get_field(POS_KEY_NAME, $a);
      $p2 = get_field(POS_KEY_NAME, $b);

      if (!$p1 || $p1 == '') { return 1; }
      else if (!$p2 || $p2 == '') { return -1; }
      if ($p1 == $p2) { return 0; }
      else if ($p1 < $p2) { return -1; }
      else { return 1; }
    });
  }

  if ($attr && array_key_exists('count', $attr)) {
    $posts = array_splice($posts, 0, $attr['count']);
  }

  $small = false;
  if ($attr && array_key_exists('small', $attr)) {
    $small = $attr['small'];
  }

  // ob allows us to echo instead of returning (so the syntax is more reasonable)
  ob_start();

?>
<div class="booker-listing flex flex-col">
  <div class="flex flex-row border-none bg-primary w-full text-white">
    <div class="w-2/6 text-center mx-4">Booker</div>
    <div class="w-2/6 text-center">Ehdot</div>
    <div class="w-2/6"></div>
  </div>

  <div>
    <?php
    global $post;
    $index = 1;
    foreach ($posts as $post):
      setup_postdata($post);
      set_query_var('is_first_element', false);
      set_query_var('is_small', $small);
      set_query_var('booker_index', $index);

      include (dirname( __FILE__ ) . '/booker-row-template.php');
      $index++;
    endforeach; ?>
    <?php wp_reset_postdata(); ?>
  </div>
</div>
<?php

  return ob_get_clean();
}

function get_booker_card ($name = '') {
  $query = new WP_Query( array(
    'name' => $name,
    'post_type' => 'booker',
    'post_status' => 'publish',
    'numberposts' => 1,
  ));

  ob_start();
  if ($query->have_posts()) {
    $query->the_post();

    set_query_var('is_first_element', false);
    set_query_var('is_small', true);

    include (dirname( __FILE__ ) . '/booker-row-template.php');
  }
  return ob_get_clean();
}

function booker_banner_ad ($name = '', $ad = '') {
  $query = new WP_Query( array(
    'name' => $name,
    'post_type' => 'booker',
    'post_status' => 'publish',
    'numberposts' => 1,
  ));

  if ($query->have_posts()):
    $query->the_post();
    $logo_id = get_field('logo');
    $url = get_field('cta');
    $logo = wp_get_attachment_image( $logo_id, '185x50', false, array('class' => 'mx-auto') ); // logos are always this size
      ?>
      <div class="w-full flex flex-row">
        <div class="text-2xl font-bold flex items-center">
          <?= $logo; ?>
        </div>
        <div class="text-md whitespace-nowrap flex items-center">
          <?= $ad; ?>
        </div>
        <a class="btn-cta bg-cta text-white darken-on-hover hover:text-white flex" title="Pelaa" href="<?= $url; ?>" >
           <span class="w-full self-center text-center text-lg font-bold uppercase px-4">
            Pelaa
          </span>
        </a>
      </div>
  <?php endif;
}

//  shortcode
//  @param small : boolean
//  @param count : int
//  TODO only bookers with a feature (the select thing)
function affi_add_shortcodes () {
  add_shortcode('booker_list', 'booker_list_shortcode');
}
add_action( 'init', 'affi_add_shortcodes' );

function affi_admin_style() {
  // all styles are combined into single file so include it
  wp_enqueue_style('affi-admin-styles', plugins_url( 'wp-admin/admin_style.css', __FILE__  ));
}
add_action('admin_enqueue_scripts', 'affi_admin_style', 0);
