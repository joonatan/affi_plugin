<?php

// need following fields:
//
// game: string
// expire time: datetime
// osuko : select (osu, huti, push)
// panostus : select (1/3, 2/3, 3/3)
// array ({booker, multiplier})

define('VIHJE_NUMBER_OF_BOOKERS', 5);

// Create a custom type for vihje
function affi_vihje_post_type() {

  $labels = array(
    'name'                => _x( 'Vihjeet', 'Post Type General Name', 'affi' ),
    'singular_name'       => _x( 'Vihje', 'Post Type Singular Name', 'affi' ),
    'menu_name'           => __( 'Vihjeet', 'affi' ),
    'parent_item_colon'   => __( 'Parent Vihje', 'affi' ),
    'all_items'           => __( 'All Vihjeet', 'affi' ),
    'view_item'           => __( 'View Vihje', 'affi' ),
    'add_new_item'        => __( 'Add New Vihje', 'affi' ),
    'add_new'             => __( 'Add New', 'affi' ),
    'edit_item'           => __( 'Edit Vihje', 'affi' ),
    'update_item'         => __( 'Update Vihje', 'affi' ),
    'search_items'        => __( 'Search Vihje', 'affi' ),
  );

  // TODO this should be changable from the admin interface
  $desc = "Urheiluvedot.com – sivustolta löydät parhaat ilmaiset pitkävetovihjeet," .
    " veikkausvihjeet ja vedonlyöntivihjeet! Vihjetiimi kasvaa kokoajan, ja tuomme" .
    " jatkuvasti lisää ammattitaitoisia asiantuntijoita tekemään työn sinun puolestasi." .
    " Urheiluvedot tarjoaa kattavan kattauksen pitkävetovihjeitä eri sarjoihin." .
    " Kattaukseemme kuuluu esimerkiksi NHL vihjeet, tennis vihjeet, MLB vihjeet, NFL vihjeet ja NBA vihjeet.";

  $args = array(
    'label'               => __( 'vihjeet', 'affi' ),
    'description'         => $desc,
    'labels'              => $labels,
    'supports'            => array( 'title', 'editor', 'author', 'thumbnail', 'revisions', 'custom-fields' ),
    'hierarchical'        => false, // no parents
    'public'              => true,
    'menu_position'       => 2,
    'has_archive'         => true, // no archive so we can have page at slug
    'capability_type'     => 'post',
    'show_in_rest'        => true,
    'menu_icon'           => 'dashicons-forms',
    'rewrite'             => array( 'slug' => 'vihjeet' ),
    'taxonomies'          => array( 'post_tag' ) // use post tags
  );

  register_post_type( 'vihje', $args );
}
add_action( 'init', 'affi_vihje_post_type', 0 );

function vihje_add_seuranta_menupage() {
  add_submenu_page( 'edit.php?post_type=vihje', __('Vihje seuranta'), __('Seuranta'), 'edit_posts', 'seuranta', 'vihje_seuranta_cb');
}
add_action('admin_menu', 'vihje_add_seuranta_menupage');

// return only the first part of the panos because we normalise the result
function calc_panos ($x) {
  $arr = explode('/', $x);
  return (double)$arr[0];
}

function calc_return($panostus, $kerroin, $osuko) {
  $panos = calc_panos($panostus);
  switch ($osuko) {
  case 'osu':
    return $kerroin * $panos;
  case 'push':
    return $panos;
  case 'huti':
  default:
    return 0;
  }
}

// $vmulti:
//  - game
//  - kerroin
//  - panostus
//  - osuko
function process_multivihje($vmulti, $name, $author, $link, $liiga, $expires) {
  return (object)[
    'expires' => $expires,
    'game' => $vmulti->game,
    'name' => $name,
    'liiga' => $liiga,
    'author' => $author,
    'kerroin' => $vmulti->kerroin,
    'panostus' => $vmulti->panostus,
    'panos' => calc_panos($vmulti->panostus),
    'osuko' => $vmulti->osuko,
    'link' => $link,
    'return' => calc_return($vmulti->panostus, $vmulti->kerroin, $vmulti->osuko)
  ];
}

function process_vihje($vihje, $author, $link, $liiga, $expires) {
  $sel_bookers = json_decode( html_entity_decode($vihje->vihje_bookers) );
  $kerroin = $sel_bookers[0]->kerroin ?? 0;

  return (object)[
    'expires' => $expires,
    'game' => $vihje->vihje_game,
    'name' => $vihje->post_title,
    'liiga' => $liiga,
    'author' => $author,
    'kerroin' => $kerroin,
    'panostus' => $vihje->vihje_panostus,
    'panos' => calc_panos($vihje->vihje_panostus),
    'osuko' => $vihje->vihje_osuko,
    'link' => $link,
    'return' => calc_return($vihje->vihje_panostus, $kerroin, $vihje->vihje_osuko)
  ];
}

/**
 * Display callback for the submenu page.
 */
function vihje_seuranta_cb() {
  // filters time, user, league (tag), not marked

  $f_author = $_GET['author'] ?? null;
  $f_from = $_GET['start_date'] ?? '';
  $f_to = $_GET['end_date'] ?? '';
  $f_marked = $_GET['marked'] ?? '';
  $f_league = $_GET['league'] ?? '';

  // TODO filter out not expired (or show them with different color)
  // TODO (low-prio) allow marking here (osuko)

  $meta_q = array();

  if ($f_author === '-1' ) {
    $f_author = null;
  }

  if ($f_from !== '') {
    // day only defaults to the start of the day
    array_push($meta_q,
      array(
        'key'     => 'vihje_expires',
        'value'   => $f_from,
        'compare' => '>=',
      )
    );
  }

  if ($f_to !== '') {
    // add the end of the day 23:59:59 to the selected day
    $exp = $f_to . ' 23:59:59';
    array_push($meta_q,
      array(
        'key'     => 'vihje_expires',
        'value'   => $exp,
        'compare' => '<=',
      )
    );
  }

  if ($f_marked == 1 || $f_marked == 2) {
    array_push($meta_q,
      array(
        'key'     => 'vihje_osuko',
        'value'   => '',
        'compare' => $f_marked == 1 ? '!=' : '==',
      )
    );
  }

  $tags = null;
  if ($f_league !== '') {
    // Partial matches if the string has *
    // Exact matches otherwise

    // remove *
    $clean_league = str_replace('*', '', $f_league);
    $tags_params = null;
    if (strpos($f_league, '*') !== false) {
      $tags_params = array( 'name__like' => $clean_league );
    } else {
      $tags_params = array( 'name' => $clean_league );
    }
    $tags = get_tags(array_merge(
      $tags_params,
      array( 'fields' => 'ids' )
    ));
  }

  // if tags are empty we didn't found any -> don't show posts
  $vihjeet = $tags !== null && empty($tags)
    ? array()
    : get_posts([
      'post_type' => 'vihje',
      'post_status' => 'publish',
      'numberposts' => -1,
      'meta_key'    => 'vihje_expires',
      'meta_type'   => 'DATETIME',
      'orderby'     => 'meta_value',
      'order'       => 'ASC',
      'author'      => $f_author,
      'meta_query'  => $meta_q,
      'tag__in'     => $tags
    ]);

  $authors = get_users();

  // leagues can't be a select because they use tags which we have 4k+
  // if we change them to custom taxonomy then we can switch it to select

  // how to calculate the return
  // if voitto => panos * kerroin
  // if push => 1
  // if huti => 0
  //
  // pal % is panos weighted avarage of returns
  //

  // Create the data array for calculating the header values
  $vihje_arr = [];
  foreach ($vihjeet as $vihje) {

    $expires = $vihje->vihje_expires;
    $link = get_edit_post_link($vihje);

    $tags = wp_get_post_tags($vihje->ID);
    $liiga = $tags[0] ?? null;

    setup_postdata($vihje);
    $author_name = get_the_author_meta( 'display_name' );

    wp_reset_postdata();

    if ($vihje->vihje_multi) {
      // loop over all the vihjeet of a multivihje
      // only the ones with game set are valid
      // add those to our results

      $multi_arr = json_decode( html_entity_decode($vihje->vihje_multi ) );
      foreach ($multi_arr as $v) {
        if ($v->game && $v->game !== '') {
          $vdata = process_multivihje($v, $vihje->post_title, $author_name, $link, $liiga, $expires);
          array_push($vihje_arr, $vdata);
        }
      }
    }

    $vdata = process_vihje($vihje, $author_name, $link, $liiga, $expires);
    array_push($vihje_arr, $vdata);
  }
  ?>
  <form class="seuranta" method="get">
    <h1><?php _e( 'Vihjeiden seuranta', 'affi' ); ?></h1>
    <div class="flex flex-wrap">
      <input name="page" type="hidden" value="seuranta"></input>
      <input name="post_type" type="hidden" value="vihje"></input>
      <div class="flex mx-4">
        <label class="mx-2">From</label>
        <input class="mx-0 bg-white" type="date" name="start_date" value=<?= $f_from; ?> ></input>
      </div>
      <div class="flex mx-4">
        <label class="mx-2">To</label>
        <input class="mx-0 bg-white" type="date" name="end_date" value=<?= $f_to; ?> ></input>
      </div>
      <select class="mx-0 bg-white" name="author">
        <option value="-1" <?= $f_author === null  ? 'selected="selected"' : ''; ?> >All authors</option>
        <?php for ($i = 0; $i < count($authors); ++$i): ?>
          <?php $a = $authors[$i]; ?>
          <?php $user_id = $authors[$i]->data->ID; ?>
          <option value="<?= $user_id; ?>" <?= ($f_author === strval($user_id)) ? 'selected' : ''; ?> ><?= esc_html( $a->display_name ); ?> </option>
        <?php endfor; ?>
      </select>
      <select class="mx-4 bg-white" name="marked">
        <option value="0">any</option>
        <option value="1" <?= $f_marked == '1' ? 'selected' : ''; ?>>marked</option>
        <option value="2" <?= $f_marked == '2' ? 'selected' : ''; ?> >not marked</option>
      </select>
      <input class="mx-4 bg-white" name="league" placeholder="All leagues" value="<?= $f_league; ?>" ></input>
      <?php submit_button('Filter', 'small', '', false); ?>
    </div>
    <h2><?php _e( 'Tilastot', 'affi' ); ?></h2>
      <?php
      $sum =  function ($a, $b) { return $a+$b; };
      $get_return = function ($x) { return $x->return; };
      $get_panos = function ($x) { return $x->panos; };
      $get_tasa = function ($x) { return $x->return / $x->panos; };

      // only calculate the percentages from marked
      $only_marked_arr = array_filter($vihje_arr, function ($x) { return $x->osuko !== ''; });

      $return_total = array_reduce(
        array_map($get_return, $only_marked_arr),
        $sum,
        0
      );

      $panos_total = array_reduce(
        array_map($get_panos, $only_marked_arr),
        $sum,
        0
      );

      $tasa_total = array_reduce(
        array_map($get_tasa, $only_marked_arr),
        $sum,
        0
      );

      $palautus_p = $panos_total > 0 ? $return_total / $panos_total : 0;
      $tasapanos_p = count($vihje_arr) > 0 ? $tasa_total / count($vihje_arr) : 0;
      ?>
    <table class="bg-white w-full border-1">
      <thead>
        <tr>
          <th>Betsit</th>
          <th>Merkkaamatta</th>
          <th>Oikein</th>
          <th>Push</th>
          <th>Väärin</th>
          <th>Prosentti</th>
          <th>Voitto</th>
          <th>Panos</th>
          <th>Tulos</th>
          <th>Pal-%</th>
          <th>Taspanospal-%</th>
        </tr>
      </thead>
      <tbody>
      <!-- calculate the totals -->
        <?php
        $oikein_n = count(array_filter($vihje_arr, function ($x) { return $x->osuko === 'osu'; } ));
        $marked_n = count($only_marked_arr)
        ?>
        <tr style="text-align: center">
          <td><?= $marked_n; ?></td>
          <td><?= count($vihje_arr) - $marked_n; ?></td>
          <td><?= $oikein_n; ?></td>
          <td><?= count(array_filter($vihje_arr, function ($x) { return $x->osuko === 'push'; } )); ?></td>
          <td><?= count(array_filter($vihje_arr, function ($x) { return $x->osuko === 'huti'; } )); ?></td>
          <td><?= $marked_n > 0 ? $oikein_n / $marked_n  * 100 : 0; ?> %</td>
          <td><?= $return_total; ?></td>
          <td><?= $panos_total; ?></td>
          <td><?= round($return_total - $panos_total, 2); ?></td>
          <td><?= $palautus_p * 100; ?> %</td>
          <td><?= $tasapanos_p * 100; ?> %</td>
        </tr>
      </tbody>
    </table>
    <h2><?php _e( 'Merkkaus', 'affi' ); ?></h2>
    <table class="bg-white w-full border-1">
      <thead>
        <tr>
        <th>Pvm</th>
        <th>Vihje</th>
        <th>Veto</th>
        <th>Author</th>
        <th>Liiga</th>
        <th>Kerroin</th>
        <th>Panos</th>
        <th>Osuko (v/h/t)</th>
        <th>Return</th>
        </tr>
      </thead>
      <tbody class="seuranta-tbody">
      <?php

        foreach ($vihje_arr as $vihje) {
          $tag = $vihje->liiga->name ?? '';
          echo '<tr style="text-align: center">';
          echo "<td>$vihje->expires</td>";
          echo "<td><a href=\"$vihje->link\">$vihje->name</a></td>";
          echo "<td>$vihje->game</td>";
          echo "<td>$vihje->author</td>";
          echo "<td>{$tag}</td>";
          echo "<td>$vihje->kerroin</td>";
          echo "<td>$vihje->panostus</td>";
          echo "<td>$vihje->osuko</td>";
          echo "<td>$vihje->return</td>";
          echo '</tr>';
        }
      ?>
      </tbody>
    </table>
  </form>
  <?php
}

function affi_vihje_metabox_callback( $post ) {
  wp_nonce_field( 'meta_vihje_save', 'page_meta_nonce' );

  $game = $post->vihje_game;
  $panostus = $post->vihje_panostus;
  $tz = new DateTimeZone('Europe/Helsinki');
  $expires = date_create($post->vihje_expires, $tz);
  $osuko = $post->vihje_osuko;
  $sel_bookers = json_decode( html_entity_decode($post->vihje_bookers) );

  // TODO error checking to the jsons

  // booker groups instead of bookers directly
  $bookers = get_terms( array(
      'taxonomy' => 'booker_group',
      'hide_empty' => false,
  ));

  $panostus_arr = array('0.5/3', '1/3', '1.5/3', '2/3', '2.5/3', '3/3');
?>
  <div class="w-2/5 mr-4">
    <div class="flex my-2 text-lg">
      <label class="w-1/2 text-xl font-semibold" for="game">Veto</label>
      <input id="game" class="w-1/2" type="text" name="game" value="<?= esc_attr( $game ); ?>" placeholder="Veto"></input>
    </div>
    <div class="flex my-2">
      <label class="w-1/2" for="expire_date">Sulkeutumis Päivä</label>
      <input id="expire_date" class="w-1/2" type="date" name="expire_date" value="<?= esc_attr( date_format($expires, 'Y-m-d' ) ); ?>" placeholder=""></input>
    </div>
    <div class="flex my-2">
      <label class="w-1/2" for="expire_time">Sulkeutumis Aika</label>
      <input id="expire_date" class="w-1/2" type="time" name="expire_time" value="<?= esc_attr( date_format($expires, 'H:i') ); ?>" placeholder=""></input>
    </div>
    <div class="flex my-2">
      <label class="w-1/2" for="panostus">Panostus</label>
      <select id="panostus" class="w-1/2" name="panostus">
        <?php foreach ($panostus_arr as $pan): ?>
          <option value="<?= $pan; ?>" <?= $pan == $panostus ? 'selected' : ''; ?> ><?= $pan; ?></option>
        <?php endforeach; ?>
      </select>
    </div>
  </div>
  <div class="w-2/5 mr-4">
    <h3 class="text-xl font-semibold">Bookers</h3>
    <div class="mx-2">
      <?php for ($i = 0; $i < VIHJE_NUMBER_OF_BOOKERS; $i++): ?>
        <?php
        $sel_booker = ($sel_bookers && count($sel_bookers) > $i) ? $sel_bookers[$i]->booker : '';
        $sel_kerroin = ($sel_bookers && count($sel_bookers) > $i) ? $sel_bookers[$i]->kerroin : '';
        ?>
        <div class="my-2 w-full flex">
          <div class="flex w-1/2 mr-4">
            <label class="mr-2 text-md font-semibold" for="booker-<?= $i; ?>"><?= $i+1; ?></label>
            <select id="booker-<?= $i; ?>" name="booker-<?= $i; ?>">
              <option value=""></option>
              <?php foreach ($bookers as $b): ?>
                <option value="<?= $b->slug; ?>" <?= ($b->slug == $sel_booker) ? 'selected' : ''; ?> ><?= $b->name; ?></option>
              <?php endforeach; ?>
            </select>
          </div>
          <div class="flex w-1/2">
            <label class="mr-2" for="kerroin-<?= $i; ?>">Kerroin</label>
            <input id="kerroin-<?= $i; ?>" type="number" step="0.01" min="0" max="99" name="kerroin-<?= $i; ?>" value="<?= $sel_kerroin; ?>" placeholder=""></input>
          </div>
        </div>
      <?php endfor; ?>
    </div>
  </div>
  <div class="w-1/5 text-lg">
    <label for="osuko">Osuko</label>
    <select id="osuko" name="osuko">
      <option value=""></option>
      <option value="huti" <?= "huti" == $osuko ? 'selected' : ''; ?> >huti</option>
      <option value="osu"  <?= "osu"  == $osuko ? 'selected' : ''; ?> >osu</option>
      <option value="push" <?= "push" == $osuko ? 'selected' : ''; ?> >push</option>
    </select>
  </div>
<?php
}

function affi_multivihje_metabox_callback( $post ) {

  $multi_arr = json_decode( html_entity_decode($post->vihje_multi ) );

  // booker groups instead of bookers directly
  $bookers = get_terms( array(
      'taxonomy' => 'booker_group',
      'hide_empty' => false,
  ));

  $tz = new DateTimeZone('Europe/Helsinki');
  $expires = date_create($post->vihje_expires, $tz);

  for ($i = 0; $i < 9; ++$i) {
    $multi = $multi_arr[$i] ?? (object)[
      'game' => '',
      'booker' => '',
      'kerroin' => '',
      'panostus' => '',
      'osuko' => ''
    ];

    $game = $multi->game;
    $panostus = $multi->panostus;
    $osuko = $multi->osuko;
    $sel_booker = $multi->booker;

    $panostus_arr = array('0.5/3', '1/3', '1.5/3', '2/3', '2.5/3', '3/3');
?>
  <div class="flex justify-between">
    <div class="flex my-2 text-lg">
      <label class="w-1/2 text-xl font-semibold" for="multivihje-<?= $i; ?>-game">Veto <?= $i + 1; ?></label>
      <input id="multivihje-<?= $i; ?>-game" class="w-1/2" type="text" name="multivihje-<?= $i; ?>-game" value="<?= esc_attr( $game ); ?>" placeholder="Veto"></input>
    </div>
    <div class="flex my-2">
      <label class="mr-2 text-md font-semibold flex align-items-center"for="multivihje-<?= $i; ?>-panostus">Panostus</label>
      <select id="multivihje-<?= $i; ?>-panostus" class="w-1/2" name="multivihje-<?= $i; ?>-panostus">
        <?php foreach ($panostus_arr as $pan): ?>
          <option value="<?= $pan; ?>" <?= $pan == $panostus ? 'selected' : ''; ?> ><?= $pan; ?></option>
        <?php endforeach; ?>
      </select>
    </div>
    <div class="mr-4 flex">
      <label class="mr-2 text-md font-semibold flex align-items-center" for="multivihje-<?= $i; ?>-booker">
        Booker
      </label>
      <select id="multivihje-<?= $i; ?>-booker" name="multivihje-<?= $i; ?>-booker">
        <option value=""></option>
        <?php foreach ($bookers as $b): ?>
          <option value="<?= $b->slug; ?>" <?= ($b->slug == $sel_booker) ? 'selected' : ''; ?> ><?= $b->name; ?></option>
        <?php endforeach; ?>
      </select>
    </div>
    <div class="mr-4 flex">
      <label class="mr-2 text-md flex align-items-center" for="multivihje-<?= $i; ?>-kerroin">
        <div>Kerroin</div>
      </label>
      <input id="multivihje-<?= $i; ?>-kerroin" type="number" step="0.01" min="0" max="99" name="multivihje-<?= $i; ?>-kerroin" value="<?= $multi->kerroin; ?>" placeholder="" size="6" maxlength="6"></input>
    </div>
    <div class="flex">
      <label class="mr-2 text-md flex align-items-center" for="multivihje-<?= $i; ?>-osuko">Osuko</label>
      <select id="multivihje-<?= $i; ?>-osuko" name="multivihje-<?= $i; ?>-osuko">
        <option value=""></option>
        <option value="huti" <?= "huti" == $osuko ? 'selected' : ''; ?> >huti</option>
        <option value="osu"  <?= "osu"  == $osuko ? 'selected' : ''; ?> >osu</option>
        <option value="push" <?= "push" == $osuko ? 'selected' : ''; ?> >push</option>
      </select>
    </div>
  </div>
<?php
  }
}

function check_write_access($post_id) {
  if ( isset( $_POST['post_type'] ) && 'page' == $_POST['post_type'] ) {
    if ( ! current_user_can( 'edit_page', $post_id ) ) {
      return false;
    }
  }
  else {
    if ( ! current_user_can( 'edit_post', $post_id ) ) {
      return false;
    }
  }
  return true;
}
// Save custom post meta data
function affi_vihje_meta_save_data( $post_id ) {
  if ( ! isset( $_POST['page_meta_nonce'] ) ) {
    return;
  }

  if ( ! wp_verify_nonce( $_POST['page_meta_nonce'], 'meta_vihje_save' ) ) {
    return;
  }

  if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
    return;
  }

  if (!check_write_access($post_id)) {
    return;
  }

  // update data
  if ( isset($_POST['game']) ) {
    $game = sanitize_text_field( $_POST['game'] );
    update_post_meta( $post_id, 'vihje_game', $game);
  }
  if ( isset($_POST['expire_date']) || isset($_POST['expire_time']) ) {
    $expire_date = sanitize_text_field( $_POST['expire_date'] );
    $expire_time = sanitize_text_field( $_POST['expire_time'] );
    $expires = $expire_date . ' ' . $expire_time;
    update_post_meta( $post_id, 'vihje_expires', $expires);
  }
  if ( isset($_POST['panostus']) ) {
    $panostus = sanitize_text_field( $_POST['panostus'] );
    update_post_meta( $post_id, 'vihje_panostus', $panostus);
  }
  if ( isset($_POST['osuko']) ) {
    $osuko = sanitize_text_field( $_POST['osuko'] );
    update_post_meta( $post_id, 'vihje_osuko', $osuko);
  }

  $bookers = array ();
  for ($i = 0; $i < VIHJE_NUMBER_OF_BOOKERS; ++$i) {
    if (isset($_POST['booker-' . $i] )) {
      $b = sanitize_text_field( $_POST['booker-' . $i] );
      $k = sanitize_text_field( $_POST['kerroin-' . $i] );
      if ($b) {
        array_push($bookers, array ( 'booker' => $b, 'kerroin' => $k ));
      }
    }
  }
  if (count($bookers) > 0) {
    update_post_meta( $post_id, 'vihje_bookers', htmlspecialchars(json_encode($bookers)) );
  }

  // these work on input field names which should be something like
  //    multi_XX_${attrib}
  // like multivihje-1-game, multivihje-1-booker
  $multi_arr = [];
  for ($i = 0; $i < 9; ++$i) {
    if ( isset($_POST["multivihje-$i-game"]) ) {
      $multi_game =  $_POST["multivihje-$i-game"] ?? '';
      $multi_booker =  $_POST["multivihje-$i-booker"] ?? '';
      $multi_kerroin =  $_POST["multivihje-$i-kerroin"] ?? '';
      $multi_panostus =  $_POST["multivihje-$i-panostus"] ?? '';
      $multi_osuko =  $_POST["multivihje-$i-osuko"] ?? '';
      $multi = (object)[
        'game' => $multi_game,
        'booker' => $multi_booker,
        'kerroin' => $multi_kerroin,
        'panostus' => $multi_panostus,
        'osuko' => $multi_osuko
      ];
      // TODO should not add if all fields are empty, but select fields create a problem for this
      array_push($multi_arr, $multi);
    }
  }

  if (count($multi_arr) > 0) {
    update_post_meta( $post_id, 'vihje_multi', json_encode($multi_arr, JSON_UNESCAPED_UNICODE));
  }
}
add_action( 'save_post', 'affi_vihje_meta_save_data');

function affi_vihje_meta_boxes() {
  $screens = ['vihje'];

  add_meta_box( 'vihje_meta', __('Vihje', 'affi'), 'affi_vihje_metabox_callback', $screens, 'normal' );
  add_meta_box( 'vihje_multi_meta', __('Multi Vihje', 'affi'), 'affi_multivihje_metabox_callback', $screens, 'normal' );
}
add_action('add_meta_boxes', 'affi_vihje_meta_boxes');

// Add custom columns to vihje
define( 'VIHJE_OSUKO_KEY_NAME', 'osuko');
define( 'VIHJE_GAME_KEY_NAME', 'game');

function add_to_vihje_columns( $column ) : array {
  $new = array();

  if (get_post_type() !== 'vihje') {
    return $column;
  }

  // add Position before date (could be after title too)
  // inserting to associate array
  foreach($column as $key => $value) {
    if($key == 'date') {
     $new[VIHJE_OSUKO_KEY_NAME] = 'Osuko';
     $new[VIHJE_GAME_KEY_NAME] = 'Peli';
    }
    $new[$key] = $value;
  }
  return $new;
}
add_filter( 'manage_posts_columns', 'add_to_vihje_columns');

function add_to_vihje_table_row( string $column_name, $post_id ) {
  if ($column_name === VIHJE_OSUKO_KEY_NAME) {
    $val = get_post($post_id)->vihje_osuko;
    switch ($val) {
    case 'osu':
      echo '<span class="osu"></span>';
      break;
    case 'huti':
      echo '<span class="huti"></span>';
      break;
    case 'push':
      echo '<span class="push"></span>';
      break;
    default:
      echo '<span class="osuma-not-set"></span>';
      break;
    }
  } else if ($column_name === VIHJE_GAME_KEY_NAME) {
    $val = get_post($post_id)->vihje_game;
    echo "<span>$val</span>";
  }
}
add_filter( 'manage_posts_custom_column', 'add_to_vihje_table_row', 10, 3 );

function vihje_quick_edit_custom_box($column_name, $screen, $name) {
  if ($column_name === VIHJE_GAME_KEY_NAME) {
    wp_nonce_field( 'meta_vihje_save', 'page_meta_nonce' );
  ?>
  <fieldset class="inline-edit-col-left">
    <div class="inline-edit-col wp-clearfix">
      <label for="vihje-game">
        <span class="title">Pelivalinta</span>
        <span class="input-text-wrap">
          <input id="vihje-game" class="ptitle" name="game" value="">
          </input>
        </span>
      </label>
    </div>
  </fieldset>
  <?php
  } else if ($column_name === VIHJE_OSUKO_KEY_NAME ) {
    wp_nonce_field( 'meta_vihje_save', 'page_meta_nonce' );
  ?>
  <fieldset class="inline-edit-col-left">
    <div class="inline-edit-col wp-clearfix">
      <label for="osuko" class="inline-edit-status alignleft">
        <span class="title">Osuko</span>
        <select id="osuko" name="osuko">
          <option value=""></option>
          <option value="huti">huti</option>
          <option value="osu">osu</option>
          <option value="push">push</option>
        </select>
      </label>
    </div>
  </fieldset>
  <?php
  } else {
    return false;
  }
}
add_action('quick_edit_custom_box', 'vihje_quick_edit_custom_box', 10, 3);

function affi_admin_enqueue_scripts( $hook ) {
  if ('edit.php' !== $hook) {
    return;
  }
  wp_enqueue_script( 'affi-admin-js', plugins_url( 'wp-admin/js/quick-edit.js', __FILE__  ) );
}
add_action( 'admin_enqueue_scripts', 'affi_admin_enqueue_scripts' );


/// -------------------------------- Add shortcodes --------------------------
// lists open vihjeet
// @param tag: only those clues, if no tag is set returns all open
// @param count: the max number of clues, if not set returns 9
// TODO filter only open ones should be configurable
// TODO should not use theme templates for front-end
function vihje_list_shortcode ($attr) {
  $count = 9;
  if ($attr && array_key_exists('count', $attr)) {
    $count = $attr['count'];
  }
  $tag = '';
  if ($attr && array_key_exists('tag', $attr)) {
    $tag = $attr['tag'];
  }

  $tz = new DateTimeZone('Europe/Helsinki');
  $posts = get_posts([
    'post_type' => 'vihje',
    'post_status' => 'publish',
    'numberposts' => $count,
    'tag' => $tag,
    'meta_key'    => 'vihje_expires',
    'meta_type'   => 'DATETIME',
    'orderby'     => 'meta_value',
    'order'       => 'ASC',
    'meta_query' => array(
      array(
        'key'     => 'vihje_expires',
        'value'   => date_format(new DateTime('NOW', $tz), "Y-m-d H:i:s"),
        'compare' => '>',
      ),
    )
  ]);

  // ob allows us to echo instead of returning (so the syntax is more reasonable)
  ob_start();
?>
<div class="vihje-list flex flex-col md:flex-row flex-wrap">
  <?php
  global $post;
  foreach ($posts as $post):
    setup_postdata($post);

    echo '<div class="max-w-full w-full md:w-1/2 xl:w-1/3 p-0 mb-4">';
    get_template_part( 'template-parts/vihje-card-element' );
    echo '</div>';
  endforeach; ?>
  <?php wp_reset_postdata(); ?>
</div>
<?php

  return ob_get_clean();
}

function multivihje_shortcode ($attr) {
  // TODO check bounds (min, max multi vihje)
  $id = 0;
  if ($attr && array_key_exists('id', $attr)) {
    $id = $attr['id'] - 1;
  }

  global $post;

  // FIXME the array contains empty elements for all the indexes
  // it should only have non-empty indexes
  $multi_arr = json_decode( html_entity_decode($post->vihje_multi ) );
  $multi = $multi_arr[$id] ?? null;

  // ob allows us to echo instead of returning (so the syntax is more reasonable)
  ob_start();
  // TODO this should never happen
  if ($multi === null) {
    echo "No $id multi vihje<br>";
  } else {
  $posts = get_posts([
    'post_type' => 'booker',
    'booker_group' => $multi->booker
  ]);

  // TODO error checking
  $post = $posts[0];

?>
  <div class="mb-12">
    <div class="flex bg-primary text-white justify-between">
      <div class="mx-2 text-xl font-bold"><?= $multi->game; ?></div>
      <div class="mx-2 text-xl font-bold"><?= $multi->panostus; ?></div>
    </div>
    <?php
      set_query_var('game_multiplier', $multi->kerroin);
      include (dirname( __FILE__ ) . '/booker-row-template.php');
      set_query_var('game_multiplier', null);
    ?>
  </div>
<?php
  }
  wp_reset_postdata();
  return ob_get_clean();
}

function affi_vihje_add_shortcodes () {
  add_shortcode('vihjeet', 'vihje_list_shortcode');
  add_shortcode('multivihje', 'multivihje_shortcode');
}
add_action( 'init', 'affi_vihje_add_shortcodes' );
