// tailwind.config.js
module.exports = {
  future: {
    removeDeprecatedGapUtilities: true,
    purgeLayersByDefault: true,
  },
  purge: {
    // enable manual purge of extra css styles
    enabled: true,
    content: ['./src/**/*.php'],
  },
  theme: {
    extend: {
      colors: {
        'primary': 'rgb(17, 78, 125)',
        'secondary': '#2067a9',
        'cta': '#f07d00',
      },
    },
    screens: {
      'sm': '640px',
      'md': '768px',
      'lg': '1024px',
      'xl': '1280px',
    },
    minWidth: {
      'orderly': '30px',
      'cta': '110px',
    },
    fontSize: {
      'sm': '0.8rem',
      'base': '1rem',
      'md': '1rem',
      'lg': '1.2rem',
      'xl': '1.5rem',
      '2xl': '2rem',
      '3xl': '2.5rem',
    },
  },
  variants: {
    // extend doesn't work for some reason
    backgroundColor: ['responsive', 'group-hover', 'focus-within', 'hover', 'focus', 'odd'],
  }
}
